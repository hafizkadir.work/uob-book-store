export const books = [
  {
    author: 'Jeff Kinney',
    title: 'Atomic Habits',
    img: 'https://images-na.ssl-images-amazon.com/images/I/71pL+3nMzfL._AC_UL600_SR600,400_.jpg',
    id: 1,
  },

  {
    author: 'Robert Greene',
    title: 'The 48 Laws of Power',
    img: 'https://images-na.ssl-images-amazon.com/images/I/61XUtQ7NTgL._AC_UL600_SR600,400_.jpg',
    id: 2,
  },
  {
    author: 'Stephanie Garber',
    title: 'A Curse for True Love',
    img: 'https://images-na.ssl-images-amazon.com/images/I/81RhySGLBrL._AC_UL600_SR600,400_.jpg',
    id: 3,
  },
  {
    author: 'Ann Patchett',
    title: 'Tom Lake',
    img: 'https://images-na.ssl-images-amazon.com/images/I/91p-s-UhmzL._AC_UL300_SR300,200_.jpg',
    id: 4,
  },
  {
    author: 'John Grisham',
    title: 'The Exchange',
    img: 'https://images-na.ssl-images-amazon.com/images/I/91-Dqdv3a8L._AC_UL600_SR600,400_.jpg',
    id: 5,
  },
  {
    author: 'Sarah J Maas',
    title: 'A court of thorns and roses',
    img: 'https://images-na.ssl-images-amazon.com/images/I/91p-s-UhmzL._AC_UL300_SR300,200_.jpg',
    id: 6,
  },
]
